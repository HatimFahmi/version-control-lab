#include <iostream>

int main() {
    float price, percentage;
    std::cout << "\nEnter the price (pounds): ";
    std::cin >> price;
    std::cout << "\nEnter the tip percentage: ";
    std::cin >> percentage;
    std::cout << "\nThe tip is " << percentage;
    std::cout << "\nThe total amount to pay is: " << price + (price * (percentage / 100));
}
