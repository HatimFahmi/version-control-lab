#include <iostream>
#include <string>

int main() {
  std::string number;
  std::cout << "\nEnter the phone number: ";
  std::cin >> number;
  std::cout << "\nThe formatted phone number is: "<<"(" + number.substr(0, 3) + ") " + number.substr(3, 4) + " " + number.substr(7, 4)<<std::endl;
}
